from __future__ import division
from loader import to_vector

__author__ = 'suraj'

import numpy as np
from scipy.special import expit
import matplotlib.pyplot as plt
from scipy.spatial.distance import cosine
import os
import time

# activation_functions = {'sigmoid': (expit, lambda a: a*(1-a)), 'hyperbolic_tangent': (, ), 'rectifier': (, )}

output_folder = "figures_" + str(time.time())


def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)
    else:
        print "directory exists"



def get_activations(activation='identity'):
    """
    factory function for the activation function and its derivative
    """
    # TODO add other activation functions
    # fx = dict(identity=(lambda x: x, lambda x: x / x),
    #           sigmoid=(expit, lambda a: a * (1 - a)),
    #           hyper_tangent = (lambda x: np.tanh(x), lambda a: 1.0 - a**2),
    #
    #           )
    fx = dict(identity=(lambda x: x, lambda x: x / x),
          sigmoid=(expit, lambda x: expit(x) * (1 - expit(x))),
          hyper_tangent = (lambda x: np.tanh(x), lambda x: 1.0 - np.tanh(x)**2),
          rectifier=(lambda x: np.log(1+np.exp(x)), expit)
          )
    return fx[activation]


def sgn(x):
    if x >= 0.5:
        return 1
    else:
        return 0


def accuracy(Y_actual, Y_predicted):
    """
    given actual and predicted Y values, return accuracy
    """
    # print np.sum(Y_actual==Y_predicted)
    # print Counter(Y_predicted)
    # print Counter(Y_actual)
    # print(len(Y_predicted),len(Y_actual))
    a = np.sum(Y_actual == Y_predicted) / len(Y_actual)
    return a * 100.0


########################################################################################################################
# plots
########################################################################################################################



def distance(t1, t2):
    """
    return tuple computing cosine distance between two
    #TODO: extract columns and compute distances
    """
    # cosine(t1,t2)

    return (cosine(t1[0],t2[0]), cosine(t1[1],t2[1]))


def histogram(l1, iter_number, hidden_layer_name, i):
    # name = str(iter_number) + "_" + str(hidden_layer_name)
    name = "activation_iter%d_layer%d_%d" % (iter_number, hidden_layer_name, i)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, )
    n, bins, patches = ax.hist(l1, bins=10, range=(min(l1), max(l1)), histtype='bar')
    plt.xlabel('Activataion values')
    plt.ylabel('Frequency')
    plt.title(r'Histogram Activation for hidden layer %s' % name)
    # plt.show()
    fig.savefig(output_folder + "/" + name + ".png", dpi=fig.dpi)


def line_graph(X, name,i):
    name = "wtchanges_layer%d_%d"%(name,i)
    fig, ax = plt.subplots()
    x = range(1,len(X)+1)
    linestyles = ['-']
    ax.plot(x, X, linestyle=linestyles[0], c="b")

    plt.xlabel("Iterations")
    plt.ylabel("Angles")
    # ax.legend()
    # plt.show()
    fig.savefig(output_folder + "/" + name + ".png", dpi=fig.dpi)


def learning_curve(training, test):
    fig, ax = plt.subplots()
    x = range(len(training))
    linestyles = ['-', '--']

    ax.plot(x, training, linestyle=linestyles[0], c="b")
    ax.plot(x, test, linestyle=linestyles[1], c="r")

    plt.xlabel("Iterations")
    plt.ylabel("Errors")
    ax.legend(["Training error", "Test error"], loc='best')    # plt.show()
    fig.savefig(output_folder + "/learning.png", dpi=fig.dpi)


########################################################################################################################

class Layer(object):
    """
    Represents layer in a neural network
    """

    def __init__(self, number_of_neurons=1, predecessor=None, successor=None, verbose=False):
        self.verbose = verbose
        self._neurons = number_of_neurons  # number of neurons in the layer
        self._predecessor = predecessor  # previous layer
        self._successor = successor  # next layer
        # weight matrix size = s_j+1 *(sj+1) randomly initialized between uniform distribution 0,1

    @property
    def successor(self):
        return self._successor

    @successor.setter
    def successor(self, successor):
        self._successor = successor

    def get_z(self, previous_a):
        self._z= np.dot(previous_a, self._theta.T)
        return self._z

    def s(self):
        return self._neurons

    def forward_propagate(self, previous_a):
        # print("Activation:", previous_a)
        a_ = self._handle_forward_propagate(previous_a)
        if self._successor:
            # X = np.hstack((np.ones((no_of_examples, 1)), X)) # add bias 1 in front of all examples
            self._successor.forward_propagate(a_)

    # computation of a for hidden layers and output layer; (different in input layer: a=x)
    def _handle_forward_propagate(self, previous_a):
        z = self.get_z(previous_a)
        return self.a(z)

    def backward_propagate(self, previous_delta):
        # print("Delta: ", previous_delta)
        delta_ = self._handle_backward_propagate(previous_delta)
        if self._predecessor:
            self._predecessor.backward_propagate(delta_)

    # do nothing for input layer, different for output layer and the hidden layers
    def _handle_backward_propagate(self, previous_delta):
        pass

    def update_weights(self):
        self._handle_update_weights()
        if self._predecessor:
            self._predecessor.update_weights()

    def _handle_update_weights(self):
        delta_w = self.learning_rate * self._delta.T.dot(self._predecessor._a)
        self._theta -= (delta_w + (self._alpha * self._weights))
        self._weights = delta_w
        # self._theta = self._theta - self.learning_rate * np.dot(self._delta.T, self._predecessor._a)
        # if not self._theta[0][0]:
        #     print("got nan")
        # print("Theta updated",self._theta)

    def plot_activations(self, iter_number, id):
        self._handle_plots(iter_number, id)
        if self._successor:
            self._successor.plot_activations(iter_number, id + 1)

    def _handle_plots(self, iter_number, id):
        histogram(self.activations_accumulator.T[2], iter_number=iter_number, hidden_layer_name=id, i=0)
        histogram(self.activations_accumulator.T[3], iter_number=iter_number, hidden_layer_name=id, i=1)

        self.activations_accumulator=None
        # print self.activations_accumulator.shape
        # i = 1
        # for column in self.activations_accumulator.T:
        #     histogram(column, iter_number=iter_number, hidden_layer_name=id, i=i)
        #     i += 1
        #     if i > 3:
        #         break

    def plot_weight_change(self, id):
        self._handle_weight_change(id)
        if self._successor:
            self._successor.plot_weight_change(id + 1)

    def _handle_weight_change(self, id):
        # print self.weight_change
        neuron_1,neuron_2=zip(*self.weight_change)

        line_graph(neuron_1, name=id,i=0)
        line_graph(neuron_2,name=id,i=1)



    def compute_angle(self):
        self._handle_compute_angle()
        if self._successor:
            self._successor.compute_angle()


    def _handle_compute_angle(self):
        distances = distance(self.angles_store, self._theta.copy())
        self.angles_store=self._theta.copy()
        self.weight_change.append(distances)


    def clear_accumation_of_activations(self):
        self._handle_clear_accumation_of_activations()
        if self._successor:
            self._successor.clear_accumation_of_activations()


    def _handle_clear_accumation_of_activations(self):
        self.activations_accumulator = None


        # def plot_weight_change(self,id):
        #     self._handle_weight_change(id)
        #     if self._successor:
        #         self._successor.plot(id+1)
        #
        # def _handle_weight_change(self,id):
        #     histogram(self.activations_accumulator,name=id)


########################################################################################################################

class InputLayer(Layer):
    """
    defines input layer in a neural network
    """

    def a(self, x):
        self._a = x
        if self.verbose:
            print("Input Layer A:", self._a)
        return x

    def _handle_forward_propagate(self, x):
        return self.a(x)

    def _handle_update_weights(self):
        """
        does nothing as it does not have any thetas to update
        """
        pass

    def _handle_plots(self, iter_number, id):
        pass

    def _handle_weight_change(self, id):
        pass

    def _handle_compute_angle(self):
        pass

    def _handle_clear_accumation_of_activations(self):
        pass

########################################################################################################################


class HiddenLayer(Layer):
    """
    defines hidden layer in a neural network
    """

    def __init__(self, number_of_neurons=1, predecessor=None, successor=None, activation_fxn='sigmoid',
                 learning_rate=0.1, alpha=0.0, verbose=False):
        super(HiddenLayer, self).__init__(number_of_neurons, predecessor, successor, verbose)

        self._theta = np.random.uniform(-1., 1., self.s() * (self._predecessor.s() + 1))
        self._theta = self._theta.reshape(self.s(), self._predecessor.s() + 1)
        # self._theta = np.random.rand(self.s(), self._predecessor.s() + 1)
        self._weights = np.zeros((self.s(), self._predecessor.s() + 1))
        self._alpha = alpha  # momentum
        self.angles_store=self._theta.copy()

        self.activations_accumulator = None

        self.weight_change = list()

        # TODO: uncomment
        # self._theta = np.ones((self.s(), self._predecessor.s() + 1))
        # self._theta = np.array([[-.3, .21, .15], [.25, -.4, .1]])

        self.g, self.g_dash = get_activations(activation_fxn)
        self.learning_rate = learning_rate

    def a(self, z):
        a_tmp = self.g(z)
        a_tmp = np.hstack((np.ones((a_tmp.shape[0], 1)), a_tmp))
        self._a = a_tmp
        if self.activations_accumulator == None:
            self.activations_accumulator = a_tmp.copy()
            # print self.activations_accumulator.shape, self._a.shape
        else:

            self.activations_accumulator = np.vstack((self.activations_accumulator, a_tmp.copy()))
            print self.activations_accumulator.shape, self._a.shape

        if self.verbose:
            print("Hidden Layer A:", self._a)
        return a_tmp

    def _handle_backward_propagate(self, previous_delta):
        # temp = self.g_dash(self._a)
        delta_tmp = np.dot(previous_delta, self._successor._theta)[:, 1:] * self.g_dash(self._z)
        # remove the delta component for bias term
        # delta_tmp = delta_tmp[:, 1:]
        self._delta = delta_tmp
        if self.verbose:
            print("Hidden Layer delta:", self._delta)
        return delta_tmp
        # self._theta += self.learning_rate * np.dot(self._delta.T, self._predecessor._a)
        # # print "hidden_theta", self._theta







########################################################################################################################


class OutputLayer(Layer):
    """
    defines output layer in a neural network
    """

    def __init__(self, number_of_neurons=1, predecessor=None, successor=None, activation_fxn='sigmoid',
                 learning_rate=0.1, alpha=0.0, verbose=False):
        super(OutputLayer, self).__init__(number_of_neurons, predecessor, successor, verbose)

        self._theta = np.random.uniform(-1., 1., self.s() * (self._predecessor.s() + 1))
        self._theta = self._theta.reshape(self.s(), self._predecessor.s() + 1)
        # self._theta = np.random.rand(self.s(), self._predecessor.s() + 1)
        self._weights = np.zeros((self.s(), self._predecessor.s() + 1))
        self._alpha = alpha  # momentum
        self.weight_change = list()
        # TODO: uncomment
        # self._theta = np.ones((self.s(), self._predecessor.s() + 1))
        # self._theta = np.array([[-.4, -.2, .3]])

        self.g, self.g_dash = get_activations(activation_fxn)
        self.learning_rate = learning_rate

    def a(self, z):
        a_tmp = self.g(z)
        # print "output_a", self._a
        self._a = a_tmp
        if self.verbose:
            print("OutputLayer A:", self._a)
        return a_tmp

    def _handle_backward_propagate(self, y):
        delta_tmp = (self._a - y)  # * self.g_dash(self._a)
        self._delta = delta_tmp
        if self.verbose:
            print("OutputLayer delta:", self._delta)
        return delta_tmp

    def _handle_plots(self, iter_number, id):
        pass

    def _handle_weight_change(self, id):
        pass

    def _handle_compute_angle(self):
        pass

    def _handle_clear_accumation_of_activations(self):
        pass


########################################################################################################################


class NeuralNetwork:
    """
    Neural Network
    """

    def __init__(self, activation_fxn="sigmoid", momentum=0, learning_rate=0.1, batch_size=10, no_of_hidden_layers=1,
                 neurons_in_hidden_layer=[50], epoch=50, verbose=False):
        create_opdir(output_folder)
        self.activation_fxn = activation_fxn
        self.momentum = momentum
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.no_of_hidden_layers = no_of_hidden_layers
        self.neurons_in_hidden_layer = neurons_in_hidden_layer
        self.epoch = epoch
        self.verbose = verbose

    def build_network(self, no_of_features, no_of_classes):
        self.input_layer = InputLayer(number_of_neurons=no_of_features, verbose=self.verbose)
        previous_layer = self.input_layer
        for i in range(self.no_of_hidden_layers):
            layer = HiddenLayer(number_of_neurons=self.neurons_in_hidden_layer[i], predecessor=previous_layer,
                                activation_fxn=self.activation_fxn, learning_rate=self.learning_rate,
                                verbose=self.verbose, alpha=self.momentum)
            previous_layer.successor = layer
            previous_layer = layer
        self.output_layer = OutputLayer(number_of_neurons=no_of_classes, predecessor=layer,
                                        activation_fxn=self.activation_fxn, learning_rate=self.learning_rate,
                                        verbose=self.verbose, alpha=self.momentum)
        layer.successor = self.output_layer

    def fit(self, X, y, X_test, y_test, no_of_classes=2):
        no_of_examples, no_of_features = X.shape
        y_vector = to_vector(y)
        self.build_network(no_of_features, 1 if no_of_classes <= 2 else no_of_classes)
        X_bias = np.hstack((np.ones((no_of_examples, 1)), X))  # add bias 1 in front of all examples

        train_error = []
        test_error = []
        for i in range(self.epoch):
            # random shuffle data

            idx = np.random.permutation(y_vector.shape[0])
            X_bias, y_vector = X_bias[idx], y_vector[idx]
            data_chunks = np.array_split(range(y_vector.shape[0]), self.batch_size)

            # start training with given batch sizes for X and y
            for ids in data_chunks:
                # print ids
                self.input_layer.forward_propagate(X_bias[ids])
                self.output_layer.backward_propagate(y_vector[ids])
                self.output_layer.update_weights()

            # compute angles between previous and new weight vectors
            self.input_layer.compute_angle()

            self.input_layer.plot_activations(i, 0)
            self.input_layer.clear_accumation_of_activations()




            print("Done Epoch: %d out of %d" % ((i + 1), self.epoch))

            y_train_predictions = self.predict(X)
            train_error.append(100.0 - accuracy(y, y_train_predictions))
            self.input_layer.clear_accumation_of_activations()

            y_test_predictions = self.predict(X_test)
            test_error.append(100.0 - accuracy(y_test, y_test_predictions))
            #
            self.input_layer.clear_accumation_of_activations()

        learning_curve(train_error, test_error)
        #plot weight change
        self.input_layer.plot_weight_change(0)


        # print "=" * 30

    def predict(self, X):
        X = np.hstack((np.ones((X.shape[0], 1)), X))  # add bias 1 in front of all examples
        self.input_layer.forward_propagate(X)
        # print self.output_layer._a
        # predicted = np.argmax(self.output_layer._a, axis=1)
        if self.output_layer.s() == 1:
            vectorize_sgn = np.vectorize(sgn)
            return vectorize_sgn(self.output_layer._a.T[0])
        # display_array(self.output_layer._a)
        return np.argmax(self.output_layer._a, axis=1)
        # print "accuracy", len([1 for a, b in zip(predicted, y) if a == b]) * 100. / len(predicted)


def display_array(arr):
    row, col = arr.shape
    for i in range(int(row / 10)):
        print arr[i]
