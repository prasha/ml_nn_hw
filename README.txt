Dependencies
========================================================================
1> numpy, scipy (matrix and vector operations)
2> scikit learn (normalization)
3> matplotlib (charts)



Run
========================================================================

1> help
=========================================================================

$ python main.py -h
Usage: main.py [options]

Options:
  -h, --help            show this help message and exit
  -p BASE_PATH, --basepath=BASE_PATH
                        path/to/mnist/folder
  -l LEARNING_RATE, --learning=LEARNING_RATE
                        learning rate, default:0.1
  -m MOMENTUM, --momentum=MOMENTUM
                        momentum constant, default:0.0
  -b BATCH, --batch=BATCH
                        batch size, default:50
  -e EPOCH, --epoch=EPOCH
                        epoch, default:50
  -i HIDDEN_LAYERS, --hiddenlayers=HIDDEN_LAYERS
                        number of hidden layers, default:1
  -n NEURONS_HIDDEN_LAYER, --neurons=NEURONS_HIDDEN_LAYER
                        number of neurons in hidden layers, passes as csv
                        eg'2,3,4' if there are 2, 3 and 4 neurons in 3 hidden
                        layers
  -o OUTPUT, --output=OUTPUT
                        path/to/output/folder : for charts
  -a ACTIVATION_FX, --activation=ACTIVATION_FX
                        [sigmoid|hyper_tangent|rectifier]  default: sigmoid


2> running experiment
===========================================================================

$ python main.py -p "data/mnist/"  -m 0.001 -l 0.001 -e 10 -b 50 -i 1 -n '50' -a "sigmoid"
Loading data
/Library/Python/2.7/site-packages/sklearn/utils/validation.py:493: UserWarning: StandardScaler assumes floating point values as input, got uint8
  "got %s" % (estimator, X.dtype))
Training ...
Done Epoch: 1 out of 10
Done Epoch: 2 out of 10
Done Epoch: 3 out of 10
Done Epoch: 4 out of 10
Done Epoch: 5 out of 10
Done Epoch: 6 out of 10
Done Epoch: 7 out of 10
Done Epoch: 8 out of 10
Done Epoch: 9 out of 10
Done Epoch: 10 out of 10
Loading test data
Testing...
('Accuracy= ', 89.810000000000002)


Surajs-Air:ml_nn_hw suraj$
