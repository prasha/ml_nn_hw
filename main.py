from sklearn.preprocessing import StandardScaler
from loader import load_data, load_mnist_data, to_vector, number_of_classes
from nn import NeuralNetwork, accuracy

__author__ = 'suraj'

import sys


def get_parser():
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-p", "--basepath", dest="base_path",
                      help="path/to/mnist/folder")
    parser.add_option("-l", "--learning", dest="learning_rate", default=0.1, type="float",
                      help="learning rate, default:0.1")
    parser.add_option("-m", "--momentum", dest="momentum", default=0.0, type="float",
                      help="momentum constant, default:0.0")
    parser.add_option("-b", "--batch", dest="batch", default=50, type="int",
                      help="batch size, default:50")
    parser.add_option("-e", "--epoch", dest="epoch", default=50, type="int",
                      help="epoch, default:50")
    parser.add_option("-i", "--hiddenlayers", dest="hidden_layers", default=1, type="int",
                      help="number of hidden layers, default:1")
    parser.add_option("-n", "--neurons", dest="neurons_hidden_layer",
                      help="number of neurons in hidden layers, passes as csv eg'2,3,4' if there are 2, 3 and 4 neurons in 3 hidden layers")
    parser.add_option("-o", "--output", dest="output",
                      help="path/to/output/folder : for charts ")
    parser.add_option("-a", "--activation", dest="activation_fx",default="sigmoid",
                      help="[sigmoid|hyper_tangent|rectifier]  default: sigmoid")


    return parser


def main(argv):
    parser = get_parser()
    (options, args) = parser.parse_args(argv)
    if not (options.base_path and options.neurons_hidden_layer):
        parser.error("Requires arguments not provided")
        parser.print_help()
        sys.exit(1)
    else:
        neurons_per_hidden_layer = [int(x) for x in options.neurons_hidden_layer.split(',')]
        if len(neurons_per_hidden_layer) != options.hidden_layers:
            print("Provided number of hidden layers and neurons per hidden layer mismatch")
            sys.exit(1)

        print("Loading data")
        X_train, y_train = load_mnist_data(options.base_path, data_type='training')
        no_classes = number_of_classes(y_train)
        #y_train = to_vector(y_train)

        noramlizer = StandardScaler()
        X_train = noramlizer.fit_transform(X_train)

        X_test, y_test = load_mnist_data(options.base_path, data_type='testing')

        X_test = noramlizer.transform(X_test)

        print("Training ...")
        nn = NeuralNetwork(learning_rate=options.learning_rate, batch_size=options.batch, epoch=options.epoch,
                           no_of_hidden_layers=options.hidden_layers,
                           neurons_in_hidden_layer=neurons_per_hidden_layer, verbose=False, momentum=options.momentum, activation_fxn=options.activation_fx)
        nn.fit(X_train, y_train, X_test,y_test,no_classes)

        print("Testing...")
        y_predicted = nn.predict(X_test)
        print("Accuracy= ", accuracy(y_test, y_predicted))


if __name__ == "__main__":
    # main(sys.argv)

    X_train, y_train = load_mnist_data('data/mnist', data_type='training')
    X_test, y_test = load_mnist_data('data/mnist', data_type='testing')

    # X_train = X_train[:10, ]
    # print X_train
    # y_train = y_train[:10, ]
    # X_test = X_test[:10, ]
    # y_test = y_test[:10, ]


    no_classes = number_of_classes(y_train)
    #y_train = to_vector(y_train)

    # noramlize the data before training
    noramlizer = StandardScaler()
    X_train = noramlizer.fit_transform(X_train)

    nn = NeuralNetwork(learning_rate=0.01, batch_size=50, epoch=50, no_of_hidden_layers=1,
                       neurons_in_hidden_layer=[50], verbose=False, momentum=0.001)
    # nn = NeuralNetwork(learning_rate=0.001, batch_size=50, epoch=5, no_of_hidden_layers=2,
    #                    neurons_in_hidden_layer=[5, 6], verbose=False, momentum=0.001)
    nn.fit(X_train, y_train, X_test, y_test, no_classes)

    # normalize
    X_test = noramlizer.transform(X_test)

    y_predicted = nn.predict(X_test)
    print("Test Accuracy= ", accuracy(y_test, y_predicted))

    # y_train_predicted=nn.predict(X_train)
    # print("Accuracy= ", accuracy(y_train,y_train_predicted))



    # # nn = NeuralNetwork(learning_rate=1, epoch=1,no_of_hidden_layers=1,neurons_in_hidden_layer=[2])
    # # X, y, y_not,no_of_classes = load_data("data/xor")
    # # nn.fit(X,y,no_of_classes)
    # # y_predicted=nn.predict(X)
    # # print("Y_actual: ",y_not)
    # # print("Y_predicted: ",y_predicted)
    # # print("Accuracy= ", accuracy(y_not,y_predicted))
    # # X, y, y_not_transformed, no_of_classes = load_data("data/xor_small")
    # # X, y, y_not_transformed, no_of_classes = load_data("data/optdigits.tra")
    #
    # X_train, y_train = load_data("/Users/suraj/Documents/workspace/ml_nn_hw/data/xor")
    # no_classes = number_of_classes(y_train)
    # # y_train = to_vector(y_train)
    #
    # # noramlize the data before training
    # noramlizer = StandardScaler()
    # X_train = noramlizer.fit_transform(X_train)
    #
    #
    # nn = NeuralNetwork(learning_rate=0.001, batch_size=2, epoch=10, no_of_hidden_layers=2,
    #                    neurons_in_hidden_layer=[3, 2], verbose=False, momentum=0.001)
    # nn.fit(X_train, y_train, X_train, y_train, no_classes)
    #
    # # X_test, y_test = load_mnist_data('data/mnist', data_type='testing')
    # # # normalize
    # # X_test = noramlizer.transform(X_test)
    #
    # y_predicted = nn.predict(X_train)
    # print("Accuracy= ", accuracy(y_train, y_predicted))

    # y_train_predicted=nn.predict(X_train)
    # print("Accuracy= ", accuracy(y_train,y_train_predicted))
