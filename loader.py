__author__ = 'suraj'
import csv
import os
import struct
import numpy as np


def number_of_classes(Y):
    """
    :param Y: list of classes as observed in training data
    :return: number of distinct classes
    """
    return len(set(Y))


def to_vector(y):
    """
    :param y: list of classes as observed in training data
    :return: vectorized form of y
    """
    classes = sorted(set(y))
    if len(classes) <= 2:
        # In[88]: x1 = np.array([1, 2, 1])
        # In[89]: x1[:, np.newaxis]
        a = np.array(y)
        return a.reshape(len(a), 1), 2
    else:
        # a = [[0] * len(classes)] * len(y)
        a = []
        for i, label in enumerate(y):
            temp = [0] * len(classes)
            temp[label] = 1
            a.append(temp)
        return np.array(a)


def load_data(fpath):
    """
    :param fpath: path to csv file whose last element is label
    :return: X,y
    """
    X, y = [], []
    with open(fpath, 'r') as csvfile:
        data = csv.reader(csvfile)
        for row in data:
            y.append(int(row[-1]))
            X.append([float(x) for x in row[0:-1]])
    return np.array(X), y


def load_mnist_data(fpath, data_type="training"):
    """
    :param fpath:  base path to mnist data
    :param data_type: training or testing
    :return: X,y
    """
    if data_type == 'training':
        X_path = os.path.join(fpath, 'train-images-idx3-ubyte')
        y_path = os.path.join(fpath, 'train-labels-idx1-ubyte')
    elif data_type == "testing":
        X_path = os.path.join(fpath, 't10k-images-idx3-ubyte')
        y_path = os.path.join(fpath, 't10k-labels-idx1-ubyte')

    with open(y_path, 'r') as f_label:
        magic, n = struct.unpack('>II', f_label.read(8)) # >II big-endian unsigned integer
        y = np.fromfile(f_label, dtype=np.uint8)

    with open(X_path, 'r') as f_data:
        magic, num, rows, cols = struct.unpack(">IIII", f_data.read(16))
        X = np.fromfile(f_data, dtype=np.uint8)
        X = X.reshape(len(y), 784) #28x28=784
    return X, y



# print load_data("data/xor")
# print (load_data('/Users/suraj/Documents/workspace/neutral_network/data/optdigits.tra'))
